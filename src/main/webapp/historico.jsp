<%-- 
    Document   : historico
    Created on : 10-05-2020
    Author     : Sthephania
--%>
<%@page import="java.lang.String"%>
<%@page import="java.util.Iterator"%>
<%@page import="Model.DbOxford"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="root.DAO.DiccionarioDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CIISA-EvaFinal</title>
    </head>
    <body>

        <!-- Default form ingreso de datos -->


        <p class="h4 mb-4"><h3><center>Diccionario Oxford</center></h3></p>

        <center><p>Listado de consultas</p></center>

        <!--aqui tengo que poner el diseño de la tabla -->
        <%
            List<DbOxford> clientes = new ArrayList<>(); //Diamond modificado

            if (request.getAttribute("dbOxford") != null) {
                clientes = (List<DbOxford>) request.getAttribute("dbOxford");

            }
            Iterator<DbOxford> itClientes = clientes.iterator();
        %> 
        <form class="text-center border border-light p-5" action="controller_editar" method="POST">
            <table class="table">
                <thead>

                <th scope="col">#Palabra</th>
                <th scope="col">Significado</th>
                <th scope="col">Fecha Consulta</th>
                </thead>
                <tbody>
                    <%while (it.DbOxford.hasNext()) {
                            DbOxford cli = DbOxford.next();%>
                    <tr>
                        <td><%=idpost%></td>
                        <td><%=palabrapost%></td>
                        <td><%=fechapost%></td>
                        <td><%=significadoget%></td>
                        

                    </tr>
                    <%}%>                
                </tbody>         
            </table>
            <!-- botones submit -->
            <a class="btn btn-info btn-" href="index.jsp">Nueva consulta</a>
         
           
        </form>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
