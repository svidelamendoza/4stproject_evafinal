/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.DAO.DiccionarioDAO;
import root.persistence.entities.DbOxford;

/**
 *
 * @author Sthephania
 */
@WebServlet(name = "Controller_add", urlPatterns = {"/controller_add"})
public class Controller_add extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller_add</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller_add at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          
        //Asignacion de los valores enviados por metodo post:
        String palabrapost = request.getParameter("palabrapost");
        String fechapost = request.getParameter("fechapost");
        
        
        //Creación Objeto Entity:
       
        DbOxford dbOxford = new DbOxford  ();
        dbOxford.setpalabrapost("palabrapost");
        dbOxford.setfechapost("fechapost");
  
      //Creación Objeto DAO:
       
       DiccionarioDAO dao = new DiccionarioDAO ();
       try {
       dao.create(dbOxford);
       Logger.getLogger("log").log(Level.INFO, "valor ID cliente: {0}", dbOxford.getidpost());
       }catch (Exception ex){
           Logger.getLogger ("log").log (Level.SEVERE, "{0}Error al ingresar dato", ex.getMessage());
           
         }
       
       request.getRequestDispatcher("salida.jsp").forward (request, response);
    }
       
  }

