
package ciisa.evafinal_oxford.services;

import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
//import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.DAO.DiccionarioDAO;
import ciisa.evafinal_oxford.exceptions.NonexistentEntityException;
import root.persistence.entities.DbOxford;

/**
 *
 * @author Sthephania
 */
@Path("/dbOxford")
public class dbOxford_Rest {
    
    //EntityManagerFactory emf = Persistence.createEntityManagerFactory("Evafinal_Oxford-PU");
    //EntityManager em;
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Evafinal_Oxford-PU");
    EntityManager em;
    DiccionarioDAO dao = new DiccionarioDAO ();

    
//Listar todas las palabras:
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response ListarPalabras() {
        
        //em = emf.createEntityManager();
        //List<DbOxford> listado = em.createNamedQuery("DbOxford.findAll").getResultList();
        
        System.out.println("Rescatando datos desde DAO");
        List<DbOxford> listado = dao.findDbOxfordEntities();
        
    return Response.ok(200).entity(listado).build();
    }

    //Busca elementos de acuerdo al ID:
    
    @GET
    @Path("/{idpost}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idpost") String idpost) {
        
        //em = emf.createEntityManager();
        //dbOxford Diccionario = em.find(Diccionario.class, buscarid);
        
        System.out.println("Busca ID desde DAO");
        DbOxford dbOxford= dao.findDbOxford(idpost);
        
    return Response.ok(200).entity(dbOxford).build();
    }
    
    //Inserta un nuevo registro:
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevaOxford(DbOxford Nueva){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(Nueva);
        em.getTransaction().commit();
        
        return "dbOxford word creada exitosamente";
    }
    
    //Editar un registro (DESACTIVADO EN ESTE PROYECTO):
    
    //@PUT
    //public Response editarOxford (DbOxford Editar) throws Exception{
        //String resultado = null;
        //em = emf.createEntityManager();
        //em.getTransaction().begin();
        //EditarMascota = em.merge(EditarMascota);
        //em.getTransaction().commit();
        
        //DbOxford equi = Editar;
        //dao.Edit(equi);
        
        //return Response.ok(Editar).build();
    }
    
    
    //Elimina un registro (DESACTIVADO EN ESTE PROYECTO):
    
    //@DELETE
    //@Path("/{deleteidpost}")
    //@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    //public Response eliminarId(@PathParam("deleteidpost") String deleteid){
        //em = emf.createEntityManager();
        //em.getTransaction().begin();
        //DbOxford Eliminar = em.getReference(Mascota.class, deleteid);
        //em.remove(Eliminar);
        //em.getTransaction().commit();
        
        //DiccionarioDAO Eliminar = dao.getEntityManager().getReference(DiccionarioDAO.class, deleteidpost);

        
        //try {
            //dao.destroy(deleteid);
           
       // } catch (NonexistentEntityException ex) {
           // Logger.getLogger(dBOxford_Rest.class.getName()).log(Level.SEVERE, null, ex);
        //return Response.ok("PalabraEliminada! \n Registro:"+Eliminar.toString()).build();
  //  }
        //return null;
           
      // }
    

