
package Model;

import java.util.Date;

public class DbOxford {


 private Long idpost = null;
 private String palabrapost;
 private String significadoget;
 private Date fechapost;
   
     public DbOxford(String palabrapost, String significadoget, Date fechapost) {
        this.palabrapost = palabrapost;
        this.significadoget = significadoget;
        this.fechapost = fechapost;
    }

    public Long getIdpost() {
        return idpost;
    }

    public void setIdpost(Long idpost) {
        this.idpost = idpost;
    }

    public String getPalabrapost() {
        return palabrapost;
    }

    public void setPalabrapost(String palabrapost) {
        this.palabrapost = palabrapost;
    }

    public String getSignificadoget() {
        return significadoget;
    }

    public void setSignificadoget(String significadoget) {
        this.significadoget = significadoget;
    }

    public Date getFechapost() {
        return fechapost;
    }

    public void setFechapost(Date fechapost) {
        this.fechapost = fechapost;
    }
 
 
 
}
