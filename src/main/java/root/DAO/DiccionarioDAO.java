/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.DAO;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ciisa.evafinal_oxford.exceptions.NonexistentEntityException;
import ciisa.evafinal_oxford.exceptions.PreexistingEntityException;
import root.persistence.entities.DbOxford;


/**
 *
 * @author Sthephania
 */
public class DiccionarioDAO implements Serializable {

    public DiccionarioDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("Evafinal_Oxford-PU");

    public DiccionarioDAO() {
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DbOxford dbOxford) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(dbOxford);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDbOxford(dbOxford.getidpost()) != null) {
                throw new PreexistingEntityException("DbOxford " + dbOxford + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    //public void edit(DbOxford dbOxford) throws NonexistentEntityException, Exception {
       // EntityManager em = null;
       // try {
       //     em = getEntityManager();
       //     em.getTransaction().begin();
       //     dbOxford = em.merge(dbOxford);
      //     em.getTransaction().commit();
      //  } catch (Exception ex) {
       //     String msg = ex.getLocalizedMessage();
       //     if (msg == null || msg.length() == 0) {
       //         String id = dbOxford.getidpost();
       //         if (findDbOxford(id) == null) {
       //             throw new NonexistentEntityException("The dbOxford with id " + id + " no longer exists.");
      //          }
      //      }
      //      throw ex;
      //  } finally {
      //      if (em != null) {
     //           em.close();
     //       }
     //   }
  //  }

    //public void destroy(String id) throws NonexistentEntityException {
        //EntityManager em = null;
        //try {
            //em = getEntityManager();
           // em.getTransaction().begin();
           // DbOxford dbOxford = null;
           // try {
            //    DbOxford = em.getReference(dbOxford.class, id);
             //   DbOxford.getidpost();
           // } catch (EntityNotFoundException enfe) {
           //     throw new NonexistentEntityException("The dbOxford with id " + id + " no longer exists.", enfe);
          //  }
          //  em.remove(dbOxford);
          //  em.getTransaction().commit();
       // } finally {
         //   if (em != null) {
          //      em.close();
         //   }
      //  }
    //}

    public List<DbOxford> findDbOxfordEntities() {
        return findDbOxfordEntities(true, -1, -1);
    }

    public List<DbOxford> findDbOxfordEntities(int maxResults, int firstResult) {
        return findDbOxfordEntities(false, maxResults, firstResult);
    }

    private List<DbOxford> findDbOxfordEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DbOxford.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DbOxford findDbOxford(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DbOxford.class, id);
        } finally {
            em.close();
        }
    }

    public int getDbOxfordCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DbOxford> rt = cq.from(DbOxford.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    
}
    
}
    
