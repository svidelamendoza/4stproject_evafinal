/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sthephania
 */
@Entity
@Table(name = "dbOxford")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DbOxford.findAll", query = "SELECT d FROM DbOxford d"),
    @NamedQuery(name = "DbOxford.findByidpost", query = "SELECT d FROM DbOxford d WHERE d.idpost = :idpost"),
    @NamedQuery(name = "DbOxford.findBypalabrapost", query = "SELECT d FROM DbOxford d WHERE d.palabrapost = :palabrapost"),
    @NamedQuery(name = "DbOxford.findByfechapost", query = "SELECT d FROM DbOxford d WHERE d.fechapost = :fechapost"),
    @NamedQuery(name = "DbOxford.findBysignificadoget", query = "SELECT d FROM DbOxford d WHERE d.significadoget = :significadoget")})
public class DbOxford implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "idpost")
    private String idpost;
    @Size(max = 2147483647)
    @Column(name = "palabrapost")
    private String palabrapost;
    @Size(max = 2147483647)
    @Column(name = "fechapost")
    private String fechapost;
    @Size(max = 2147483647)
    @Column(name = "significadoget")
    private String significadoget;

    public DbOxford() {
    }

    public DbOxford(String idpost) {
        this.idpost = idpost;
    }

    public String getIdpost() {
        return idpost;
    }

    public void setIdpost(String idpost) {
        this.idpost = idpost;
    }

    public String getPalabrapost() {
        return palabrapost;
    }

    public void setPalabrapost(String palabrapost) {
        this.palabrapost = palabrapost;
    }

    public String getFechapost() {
        return fechapost;
    }

    public void setFechapost(String fechapost) {
        this.fechapost = fechapost;
    }

    public String getSignificadoget() {
        return significadoget;
    }

    public void setSignificadoget(String significadoget) {
        this.significadoget = significadoget;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpost != null ? idpost.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DbOxford)) {
            return false;
        }
        DbOxford other = (DbOxford) object;
        if ((this.idpost == null && other.idpost != null) || (this.idpost != null && !this.idpost.equals(other.idpost))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.DbOxford[ idpost=" + idpost + " ]";
    }

    public void setidpost(String idpost) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setpalabrapost(String palabrapost) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setfechapost(String fechapost) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setsignificadoget(String significadoget) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getidpost() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
